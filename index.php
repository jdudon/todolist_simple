<?php
session_start();
if (isset($_POST["create"])) {

    if (isset($_SESSION["tasks"])) {
        $_SESSION["tasks"][$_POST['title']] = $_POST['description'];
    } else {
        $_SESSION["tasks"] = [];
    }
} elseif ($_GET) {
    foreach ($_SESSION["tasks"] as $title => $desc) {
        if ($title == $_GET["title"]) {
            echo "trouvé! " . $_SESSION["tasks"][$title];
            unset($_SESSION["tasks"][$title]);
            echo "Supprimé! ";
            var_dump($_SESSION["tasks"]);
        }
    }
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="" method="post">
        <input type="text" placeholder="Insérer un titre de tâche" name="title">
        <textarea name="description" id="" cols="30" rows="10"></textarea>
        <button type="submit" name="create">Créer</button>
    </form>
    <?php foreach ($_SESSION["tasks"] as $title => $desc) : ?>
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title"><?= $title ?></h5>
                <p class="card-text"><?= $desc ?></p>
                <form action="" method="get">
                    <input type="hidden" name="title" value="<?= $title ?>">
                    <input class="card-link" name="delete" type="submit" value="supprimer">
                </form>
            </div>
        </div>
    <?php endforeach ?>
</body>

</html>